
/* DE1 SoC Breakout
 * A version of the arcade game "Breakout" for the DE1 development board
 * Written by Kyle Kern <kakern16@my.trine.edu>
 * 
 * vga_controller.v:
 *   Displays an 80x60 image on a 640x480 VGA signal */

module vga_controller(
	input             iRST_n,	// Reset signal
	input             iVGA_CLK,	// 50 MHz reference clock
	input      [ 2:0] rbus,		// Game data memory read bus
	output reg        outBlank_n,	// VGA Blank signal
	output reg        outHS,		// VGA horizontal sync signal
	output reg        outVS,		// VGA vertical sync signal
	output reg [ 7:0] b_data,	// VGA Blue data bus
	output reg [ 7:0] g_data,	// VGA Green data bus
	output reg [ 7:0] r_data,	// VGA Red data bus
	output reg [12:0] raddr		// Game data memory read address
);
	// Internal signals:
	wire rst;
	wire posBlank_n, negHS, negVS; // Negative-edge-pipelined sync signals
	reg  negBlank_n, posHS, posVS; // Positive-edge-pipelined sync signals
	
	// Generate the horizontal and vertical sync signals:
	assign rst = ~iRST_n;
	video_sync_generator LTM_ins (
		.vga_clk(iVGA_CLK),
		.reset(rst),
		.blank_n(posBlank_n),
		.HS(negHS),
		.VS(negVS)
	);
	
	// Counters:
	// Note: None of these counters include the blanking intervals
	reg [9:0] all_pixels;  // Counts all transmitted pixels in a line, including the front and back porches
	reg [9:0] vis_pixels;  // Counts only the active-video-region pixels in a line
	reg [3:0] sub_pixels;  // Counts single-pixels that compose a "blocky" pixel
	reg [6:0] blk_pixels;  // Counts "blocky" pixels consisting of 8 pixels within a line
	reg [9:0] all_lines;   // Counts all transmitted lines, including during the front and back porches
	reg [8:0] vis_lines;   // Counts only the active-video-region lines
	reg [3:0] sub_lines;   // Counts single-pixel-wide lines that compose a "blocky" line
	reg [5:0] blk_lines;   // Counts "blocky" lines consisting of 8px-wide "real" lines
	reg       out_select;  // Selects which of the two block-line buffers is used for output to the display
	
	// Counter initial values:
	initial begin
		all_pixels = 0;
		vis_pixels = 0;
		sub_pixels = 0;
		blk_pixels = 0;
		all_lines = 0;
		vis_lines = 0;
		sub_lines = 0;
		blk_lines = 0;
		out_select = 0;
	end
	
	// Line counts are based on the sync pulses:
	always @ (negedge negVS or negedge negHS) begin
		// Vertical sync signals a new frame, reset all line counts:
		if (negVS == 1'b0) begin
			all_lines = 10'd0;
			vis_lines =  9'd0;
			sub_lines =  4'd0;
			blk_lines =  6'd0;
		end
		
		// Otherwise, count all lines like normal:
		else begin
			all_lines = all_lines + 10'd1;
			if (all_lines >= 10'd523) begin
				all_lines = 10'd0;  // Maximum value is 522 (480 visible + 10 front porch + 33 back porch)
			end
			else begin
				// Only advance the visible line counters when not in the front or back porch:
				if ((all_lines > 10'd32) && (all_lines < 10'd513)) begin
					vis_lines = vis_lines + 9'd1;
					
					sub_lines = sub_lines + 4'd1;
					if (sub_lines > 4'd7) begin
						sub_lines = 4'd0;              // Maximum value is 7 (only 8 lines in one "blocky" line)
						blk_lines = blk_lines + 6'd1;  // Overflowing sub-counter means it's time to increase the blocky count
						out_select = ~out_select;      // Also flip to the next line buffer
					end
				end
			end
		end
	end
	
	// Pixel counts are based on the horizontal sync pulse and the VGA clock:
	always @ (negedge negHS or negedge iVGA_CLK) begin
		// Horizontal sync signals a new line, reset all pixel counts:
		if (negHS == 1'b0) begin
			all_pixels = 10'd0;
			vis_pixels = 10'd0;
			sub_pixels =  4'd0;
			blk_pixels =  7'd0;
		end
		
		// Otherwise, count all pixels like normal:
		else begin
			all_pixels = all_pixels + 10'd1;
			if (all_pixels >= 10'd704) begin
				all_pixels = 10'd0;  // Maximum value is 703 (640 visible + 16 front porch + 48 back porch)
			end
			else begin
				// Only advance the visible counters when not in the front or back porch:
				if ((all_pixels > 10'd47) && (all_pixels < 10'd688)) begin
					vis_pixels = vis_pixels + 10'd1;
					
					sub_pixels = sub_pixels + 4'd1;
					if (sub_pixels > 4'd7) begin
						sub_pixels = 4'd0;               // Maximum value is 7 (only 8 pixels in one "blocky" column width)
						blk_pixels = blk_pixels + 7'd1;  // Overflowing sub-counter means it's time to increase the blocky count
					end
				end
			end
		end
	end
	
	// Drive timing/memory signals according to the VGA clock:
	always @ (negedge iVGA_CLK) begin
		// Calculate the memory address to read based on the current position on the screen:
		raddr = (80 * (blk_lines + 1)) + blk_pixels;
		if (raddr > 4799) raddr = raddr - 4800;
		
		// Pipeline the horizontal and vertical sync signals:
		posHS <= negHS;
		posVS <= negVS;
		negBlank_n <= posBlank_n;
	end
	
	// Line buffer operation:
	//   The words within the buffer are 3'b wide (same size as game memory locations)
	//   There are two buffers: Input from the memory, and output to the display
	//     out_select switches their purpose after every 8px-wide row
	//   Each buffer stores an entire row of 80 columns (numbered 0 through 79)
	reg [2:0] display_buffer [0:1] [0:79];
	always @ (posedge iVGA_CLK) begin
		// Read the current pixel's color into the current column's slot in the input buffer:
		display_buffer[~out_select][blk_pixels] <= rbus; // The read address is assigned on the negative edge above
		
		// Drive the inputs to the VGA connector's ADCs using values from the current column's output buffer:
		r_data <= {8{display_buffer[out_select][blk_pixels][2]}};
		g_data <= {8{display_buffer[out_select][blk_pixels][1]}};
		b_data <= {8{display_buffer[out_select][blk_pixels][0]}};
		
		// Drive the horizontal and vertical sync signals:
		outHS <= posHS;
		outVS <= posVS;
		outBlank_n <= negBlank_n;
	end
endmodule
