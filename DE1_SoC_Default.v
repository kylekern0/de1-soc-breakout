// ============================================================================
// Copyright (c) 2013 by Terasic Technologies Inc.
// ============================================================================
//
// Permission:
//
//   Terasic grants permission to use and modify this code for use
//   in synthesis for all Terasic Development Boards and Altera Development 
//   Kits made by Terasic.  Other use of this code, including the selling 
//   ,duplication, or modification of any portion is strictly prohibited.
//
// Disclaimer:
//
//   This VHDL/Verilog or C/C++ source code is intended as a design reference
//   which illustrates how these types of functions can be implemented.
//   It is the user's responsibility to verify their design for
//   consistency and functionality through the use of formal
//   verification methods.  Terasic provides no warranty regarding the use 
//   or functionality of this code.
//
// ============================================================================
//           
//  Terasic Technologies Inc
//  9F., No.176, Sec.2, Gongdao 5th Rd, East Dist, Hsinchu City, 30070. Taiwan
//  
//  
//                     web: http://www.terasic.com/  
//                     email: support@terasic.com
//
// ============================================================================
//Date:  Mon Jun 17 20:35:29 2013
// ============================================================================

//`define ENABLE_HPS

module DE1_SoC_Default(

      ///////// ADC /////////
      output             ADC_CONVST,
      output             ADC_DIN,
      input              ADC_DOUT,
      output             ADC_SCLK,

      ///////// AUD /////////
      input              AUD_ADCDAT,
      inout              AUD_ADCLRCK,
      inout              AUD_BCLK,
      output             AUD_DACDAT,
      inout              AUD_DACLRCK,
      output             AUD_XCK,

      ///////// CLOCK2 /////////
      input              CLOCK2_50,

      ///////// CLOCK3 /////////
      input              CLOCK3_50,

      ///////// CLOCK4 /////////
      input              CLOCK4_50,

      ///////// CLOCK /////////
      input              CLOCK_50,

      ///////// DRAM /////////
      output      [12:0] DRAM_ADDR,
      output      [1:0]  DRAM_BA,
      output             DRAM_CAS_N,
      output             DRAM_CKE,
      output             DRAM_CLK,
      output             DRAM_CS_N,
      inout       [15:0] DRAM_DQ,
      output             DRAM_LDQM,
      output             DRAM_RAS_N,
      output             DRAM_UDQM,
      output             DRAM_WE_N,

      ///////// FAN /////////
      output             FAN_CTRL,

      ///////// FPGA /////////
      output             FPGA_I2C_SCLK,
      inout              FPGA_I2C_SDAT,

      ///////// GPIO /////////
      inout     [35:0]         GPIO_0,
      inout     [35:0]         GPIO_1,
 

      ///////// HEX0 /////////
      output      [6:0]  HEX0,

      ///////// HEX1 /////////
      output      [6:0]  HEX1,

      ///////// HEX2 /////////
      output      [6:0]  HEX2,

      ///////// HEX3 /////////
      output      [6:0]  HEX3,

      ///////// HEX4 /////////
      output      [6:0]  HEX4,

      ///////// HEX5 /////////
      output      [6:0]  HEX5,

`ifdef ENABLE_HPS
      ///////// HPS /////////
      inout              HPS_CONV_USB_N,
      output      [14:0] HPS_DDR3_ADDR,
      output      [2:0]  HPS_DDR3_BA,
      output             HPS_DDR3_CAS_N,
      output             HPS_DDR3_CKE,
      output             HPS_DDR3_CK_N,
      output             HPS_DDR3_CK_P,
      output             HPS_DDR3_CS_N,
      output      [3:0]  HPS_DDR3_DM,
      inout       [31:0] HPS_DDR3_DQ,
      inout       [3:0]  HPS_DDR3_DQS_N,
      inout       [3:0]  HPS_DDR3_DQS_P,
      output             HPS_DDR3_ODT,
      output             HPS_DDR3_RAS_N,
      output             HPS_DDR3_RESET_N,
      input              HPS_DDR3_RZQ,
      output             HPS_DDR3_WE_N,
      output             HPS_ENET_GTX_CLK,
      inout              HPS_ENET_INT_N,
      output             HPS_ENET_MDC,
      inout              HPS_ENET_MDIO,
      input              HPS_ENET_RX_CLK,
      input       [3:0]  HPS_ENET_RX_DATA,
      input              HPS_ENET_RX_DV,
      output      [3:0]  HPS_ENET_TX_DATA,
      output             HPS_ENET_TX_EN,
      inout       [3:0]  HPS_FLASH_DATA,
      output             HPS_FLASH_DCLK,
      output             HPS_FLASH_NCSO,
      inout              HPS_GSENSOR_INT,
      inout              HPS_I2C1_SCLK,
      inout              HPS_I2C1_SDAT,
      inout              HPS_I2C2_SCLK,
      inout              HPS_I2C2_SDAT,
      inout              HPS_I2C_CONTROL,
      inout              HPS_KEY,
      inout              HPS_LED,
      inout              HPS_LTC_GPIO,
      output             HPS_SD_CLK,
      inout              HPS_SD_CMD,
      inout       [3:0]  HPS_SD_DATA,
      output             HPS_SPIM_CLK,
      input              HPS_SPIM_MISO,
      output             HPS_SPIM_MOSI,
      inout              HPS_SPIM_SS,
      input              HPS_UART_RX,
      output             HPS_UART_TX,
      input              HPS_USB_CLKOUT,
      inout       [7:0]  HPS_USB_DATA,
      input              HPS_USB_DIR,
      input              HPS_USB_NXT,
      output             HPS_USB_STP,
`endif /*ENABLE_HPS*/

      ///////// IRDA /////////
      input              IRDA_RXD,
      output             IRDA_TXD,

      ///////// KEY /////////
      input       [3:0]  KEY,

      ///////// LEDR /////////
      output      [9:0]  LEDR,

      ///////// PS2 /////////
      inout              PS2_CLK,
      inout              PS2_CLK2,
      inout              PS2_DAT,
      inout              PS2_DAT2,

      ///////// SW /////////
      input       [9:0]  SW,

      ///////// TD /////////
      input              TD_CLK27,
      input      [7:0]  TD_DATA,
      input             TD_HS,
      output             TD_RESET_N,
      input             TD_VS,

      ///////// VGA /////////
      output      [7:0]  VGA_B,
      output             VGA_BLANK_N,
      output             VGA_CLK,
      output      [7:0]  VGA_G,
      output             VGA_HS,
      output      [7:0]  VGA_R,
      output             VGA_SYNC_N,
      output             VGA_VS
);



//=======================================================
//  REG/WIRE declarations
//=======================================================


//	For Audio CODEC
wire		   AUD_CTRL_CLK;	//	For Audio Controller

//	For VGA Controller
wire		   VGA_CTRL_CLK;
wire  [9:0]	mVGA_R;
wire  [9:0]	mVGA_G;
wire  [9:0]	mVGA_B;
wire [19:0]	mVGA_ADDR;

wire			mVGA_CLK;
wire	[9:0]	mRed;
wire	[9:0]	mGreen;
wire	[9:0]	mBlue;
wire			VGA_Read;	//	VGA data request

wire  [9:0] recon_VGA_R;
wire  [9:0] recon_VGA_G;
wire  [9:0] recon_VGA_B;

wire		   DLY_RST;
reg  [31:0]	Cont;
wire [23:0]	mSEG7_DIG;

wire			mDVAL;

//audio count
reg [31:0] audio_count;
reg        key1_reg;

// Kyle's memory module wires:
wire [12:0] raddr1, raddr2, waddr;
wire [2:0] rbus1, rbus2, wbus;
wire wen;

// Game state signals:
wire [1:0] state;
wire [7:0] score;
wire [1:0] balls;
wire       partrst;

//=======================================================
//  Structural coding
//=======================================================

// initial //  
	         
assign DRAM_DQ 			= 16'hzzzz;

/*
always@(posedge CLOCK_50 or negedge KEY[0])
    begin
        if(!KEY[0])
			 Cont	<=	0;
        else
			 Cont	<=	Cont+1;
    end
	 

always@(posedge CLOCK_50)
    begin
			 key1_reg	<=	KEY[1];
			 //kr
        if(key1_reg & (!KEY[1]))
		    audio_count = audio_count + 1;
    end	 


assign	LEDR      	=	KEY[0]? {	Cont[25:24],Cont[25:24],Cont[25:24],Cont[25:24],Cont[25:24]	}:10'h3ff;
assign	mSEG7_DIG	=	KEY[0]? {	Cont[27:24],Cont[27:24],Cont[27:24],Cont[27:24],Cont[27:24],Cont[27:24] } :{6{4'b1000}};
*/

//	Reset Delay Timer
Reset_Delay			r0	(	
							 .iCLK(CLOCK_50),
							 .oRESET(DLY_RST));
//	 Audio VGA PLL clock							 


VGA_Audio u1(
		                .refclk(CLOCK_50),   //  refclk.clk
		                .rst(~DLY_RST),      //   reset.reset
		                .outclk_0(VGA_CTRL_CLK), // outclk0.clk
		                .outclk_1(AUD_CTRL_CLK), // outclk1.clk
		                .outclk_2(mVGA_CLK), // outclk2.clk
		                .locked()    //  locked.export
	);

	
videomemory vmem0 (
	//.rclk1  (VGA_CTRL_CLK),
	.rclk1   (CLOCK_50),
	.rclk2   (CLOCK_50),
	.wclk    (VGA_CTRL_CLK),
	.raddr1  (raddr1),
	.raddr2  (raddr2),
	.waddr   (waddr),
	.wbus    (wbus),
	.wen     (wen),
	.rbus1   (rbus1),
	.rbus2   (rbus2),
	.reset   (KEY[3]),
	.partrst (partrst)
);

assign VGA_CLK = VGA_CTRL_CLK;
vga_controller vga_ins(
	.iRST_n     (DLY_RST),
   .iVGA_CLK   (VGA_CTRL_CLK),
   .outBlank_n (VGA_BLANK_N),
   .outHS      (VGA_HS),
   .outVS      (VGA_VS),
   .b_data     (VGA_B),
   .g_data     (VGA_G),
   .r_data     (VGA_R),
	.raddr      (raddr2),
	.rbus       (rbus2)
);	


gamectrl gctrl0 (
	// External clock sources:
	.iVGA_CLK(VGA_CTRL_CLK),     // VGA Clock
	
	// Game controls:
	// Note: Buttons are normally high
	.iReset_Btn  (KEY[3]),   // HW button used to restart the game
	.iLeft_Btn   (KEY[2]),   // HW button used to move the paddle to the left
	.iLaunch_Btn (KEY[1]),   // HW button used to start the game
	.iRight_Btn  (KEY[0]),   // HW button used to move the paddle to the right
	
	// Game memory read interface:
	.iR_Bus      (rbus1),    // Bus for data read from the memory
	.oR_Addr     (raddr1),   // Memory address to read
	
	// Game memory write interface:
	.oW_Bus      (wbus),     // Bus for data to write to the memory
	.oW_Addr     (waddr),    // Memory address to write
	.oW_En       (wen),      // Memory write enable
	
	// Game state outputs:
	.oState      (state),    // Current state of the game (waiting, active, over)
	.oScore      (score),    // Current game score
	.oBalls      (balls),    // Current number of balls remaining
	.oP_Rst      (partrst),  // Partial reset for replacing bat and ball after ball loss
);

// Drive the DE1-SoC's on-board LEDs to display the balls remaining:
bin_to_nhot balls_nhot (
	.iBin  (balls),
	.oNHot (LEDR[2:0])
);

// Drive the DE1-SoC's on-board 7-segment array to display text according to the game state:
hex_mux text_disp_mux (
	.iState (state),
	.iScore (score),
	.oText0 (HEX0),
	.oText1 (HEX1),
	.oText2 (HEX2),
	.oText3 (HEX3),
	.oText4 (HEX4),
	.oText5 (HEX5)
);
							 
endmodule

// Converts a two-bit binary-encoded value (n) to three-bit n-hot encoding:
module bin_to_nhot (
	input      [1:0] iBin,  // Binary-encoded number
	output reg [2:0] oNHot  // One-hot-encoded number
);
	always @ (iBin) begin
		case (iBin)
			2'd0: oNHot = 3'b000;
			2'd1: oNHot = 3'b001;
			2'd2: oNHot = 3'b011;
			2'd3: oNHot = 3'b111;
		endcase
	end
endmodule

// Select between showing the game score and instructions based on the game state:
module hex_mux (
	input      [1:0]  iState,  // Game state; 0 = paused, 1 = active, 2 = over
	input      [20:0] iScore,  // Game score, binary format
	
	// 7-Segment display digits; 0 is rightmost
	output reg [ 6:0] oText0,
	output reg [ 6:0] oText1,
	output reg [ 6:0] oText2,
	output reg [ 6:0] oText3,
	output reg [ 6:0] oText4,
	output reg [ 6:0] oText5
);
	// Wires for carrying the hex-driver signals for the score:
	wire [6:0] TextScore0, TextScore1, TextScore2, TextScore3, TextScore4, TextScore5;
	
	// Use Altera's 7-segment LUT to convert the score value to hex driver signals:
	SEG7_LUT_6 u0 (
		.iDIG  (iScore),
		.oSEG0 (TextScore0),
		.oSEG1 (TextScore1),
		.oSEG2 (TextScore2),
		.oSEG3 (TextScore3),
		.oSEG4 (TextScore4),
		.oSEG5 (TextScore5)
	);
	
	// Characters for writing "LAunch" while the game is paused:
	parameter [6:0] TextPause5 = ~ (7'b0111000);  // L
	parameter [6:0] TextPause4 = ~ (7'b1110111);  // A
	parameter [6:0] TextPause3 = ~ (7'b0011100);  // u
	parameter [6:0] TextPause2 = ~ (7'b1010100);  // n
	parameter [6:0] TextPause1 = ~ (7'b1011000);  // c
	parameter [6:0] TextPause0 = ~ (7'b1110100);  // h
	
	
	// Characters for writing "rESEt" when the game is over:
	parameter [6:0] TextOver5 = ~ (7'b1010000);  // r
	parameter [6:0] TextOver4 = ~ (7'b1111001);  // E
	parameter [6:0] TextOver3 = ~ (7'b1101101);  // S
	parameter [6:0] TextOver2 = ~ (7'b1111001);  // E
	parameter [6:0] TextOver1 = ~ (7'b1111000);  // t
	parameter [6:0] TextOver0 = ~ (7'b0000000);  //  
	
	// Select between the possible hex display drivers:
	always @ (*) begin
		case (iState)
			// Paused:
			2'd0: begin
				oText0 = TextPause0;
				oText1 = TextPause1;
				oText2 = TextPause2;
				oText3 = TextPause3;
				oText4 = TextPause4;
				oText5 = TextPause5;
			end
			
			// Active (Score):
			2'd1: begin
				oText0 = TextScore0;
            oText1 = TextScore1;
            oText2 = TextScore2;
            oText3 = TextScore3;
            oText4 = TextScore4;
            oText5 = TextScore5;
			end
			
			// Over:
			2'd2: begin
				oText0 = TextOver0;
            oText1 = TextOver1;
            oText2 = TextOver2;
            oText3 = TextOver3;
            oText4 = TextOver4;
            oText5 = TextOver5;
			end
		endcase
	end
endmodule
