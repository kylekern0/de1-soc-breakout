
// Convert a 20-bit binary value to a 24-bit BCD value
/* Based on an algorithm retrieved from:
https://pubweb.eng.utah.edu/~nmcdonal/Tutorials/BCDTutorial/BCDConversion.html
*/
module bin20_to_bcd24 (
	input [19:0] BIN,
	output reg [23:0] BCD
);
	integer i;
	always @ (*) begin
		BCD = {4'h0, BIN};
		for (i = 0; i < 8; i = i + 1) begin
			// Add 3 to each "column" if >= 5
			if (BCD[23:20] >= 5) BCD[23:20] = BCD[23:20] + 4'h3;
			if (BCD[19:16] >= 5) BCD[19:16] = BCD[19:16] + 4'h3;
			if (BCD[15:12] >= 5) BCD[15:12] = BCD[15:12] + 4'h3;
			if (BCD[11:8] >= 5) BCD[11:8] = BCD[11:8] + 4'h3;
			if (BCD[7:4] >= 5) BCD[7:4] = BCD[7:4] + 4'h3;
			if (BCD[3:0] >= 5) BCD[3:0] = BCD[3:0] + 4'h3;
			
			// Shift left once:
			BCD = BCD << 1;
		end
	end
endmodule
