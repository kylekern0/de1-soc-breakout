transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+/home/kyle/School/Logic\ Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V {/home/kyle/School/Logic Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V/VGA_Audio.vo}
vlog -vlog01compat -work work +incdir+/home/kyle/School/Logic\ Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V {/home/kyle/School/Logic Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V/VGA_Audio.v}
vlog -vlog01compat -work work +incdir+/home/kyle/School/Logic\ Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V {/home/kyle/School/Logic Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V/VGA_Audio.vo}
vlog -vlog01compat -work work +incdir+/home/kyle/School/Logic\ Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V {/home/kyle/School/Logic Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V/VGA_Audio.v}
vlog -vlog01compat -work work +incdir+/home/kyle/School/Logic\ Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V {/home/kyle/School/Logic Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V/SEG7_LUT_6.v}
vlog -vlog01compat -work work +incdir+/home/kyle/School/Logic\ Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V {/home/kyle/School/Logic Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V/SEG7_LUT.v}
vlog -vlog01compat -work work +incdir+/home/kyle/School/Logic\ Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V {/home/kyle/School/Logic Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V/Reset_Delay.v}
vlog -vlog01compat -work work +incdir+/home/kyle/School/Logic\ Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V {/home/kyle/School/Logic Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V/img_data.v}
vlog -vlog01compat -work work +incdir+/home/kyle/School/Logic\ Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V {/home/kyle/School/Logic Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V/video_sync_generator.v}
vlog -vlog01compat -work work +incdir+/home/kyle/School/Logic\ Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V {/home/kyle/School/Logic Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V/vga_controller.v}
vlog -vlog01compat -work work +incdir+/home/kyle/School/Logic\ Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default {/home/kyle/School/Logic Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/DE1_SoC_Default.v}
vlog -vlog01compat -work work +incdir+/home/kyle/School/Logic\ Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V {/home/kyle/School/Logic Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V/videomemory.v}
vlog -vlog01compat -work work +incdir+/home/kyle/School/Logic\ Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default {/home/kyle/School/Logic Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/checkers.v}
vlib VGA_Audio
vmap VGA_Audio VGA_Audio
vlog -vlog01compat -work VGA_Audio +incdir+/home/kyle/School/Logic\ Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V/VGA_Audio {/home/kyle/School/Logic Lab/DE1-SoC_v.5.1.2_HWrevF.revG_SystemCD/Demonstrations/FPGA/DE1_SoC_Default/V/VGA_Audio/VGA_Audio_0002.v}

