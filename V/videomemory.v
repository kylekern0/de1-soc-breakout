
/* DE1 SoC Breakout
 * A version of the arcade game "Breakout" for the DE1 development board
 * Written by Kyle Kern <kakern16@my.trine.edu>
 * 
 * videomemory.v:
 *   Custom video/game memory interface featuring three ports */

module videomemory (
	// Clocks:
	input             rclk1,   // Read clock 1
	input             rclk2,   // Read clock 2
	input             wclk,    // Write clock
	
	// Address buses:
	input      [12:0] raddr1,  // Read address 1
	input      [12:0] raddr2,  // Read address 2
	input      [12:0] waddr,   // Write address
	
	// Data buses:
	input      [ 2:0] wbus,    // Write data bus
	output reg [ 2:0] rbus1,   // Read data bus 1
	output reg [ 2:0] rbus2,   // Read data bus 2
	
	// Control signals:
	input             wen,     // Write enable
	input             reset,   // Drive high to return memory to initial contents
	input             partrst  // Partial reset - Drive high to only reset the last two lines
);
	// Game object definitions:
	parameter [2:0] BACKGND = 3'b000;
	parameter [2:0] SIDEWALL = 3'b001;
	parameter [2:0] TOPWALL = 3'b010;
	parameter [2:0] BRICK = 3'b101;
	parameter [2:0] BALL = 3'b110;
	parameter [2:0] PADDLE = 3'b111;
	
	// Memory:
	// Only three bits are needed per word - one for each color
	// 4800 words (80 by 60 display) are needed - therefore 13-bit addresses are required
	reg [2:0] memory [4799:0];
	
	// Used in initialization/reset loops:
	integer col;
	integer row;
	
	// Delay the write input signals:
	reg [12:0] waddr_reg;
	reg [ 2:0] wbus_reg;
	reg        wen_reg;
	
	// Write data at the positive edge of the write clock:
	always @ (posedge wclk or negedge reset) begin
		// We can only drive the memory from one always block, so we must hijack this one to perform a reset.
		// Reset takes priority over normal writing:
		if (reset == 1'b0) begin
			// Top row is a solid top wall:
			for (row = 0; row < 1; row = row + 1) begin
				for (col = 0; col < 80; col = col + 1) begin
					memory[(row * 80) + col] <= TOPWALL;
				end
			end
			// Next 5 rows are empty, but place side walls at the side:
			for (row = 1; row < 6; row = row + 1) begin
				for (col = 0; col < 80; col = col + 1) begin
					if ((col == 0) || (col == 79)) memory[(row * 80) + col] <= SIDEWALL;
					else memory[(row * 80) + col] <= BACKGND;
				end
			end
			// Next 10 rows are filled with bricks:
			for (row = 6; row < 16; row = row + 1) begin
				for (col = 0; col < 80; col = col + 1) begin
					if ((col == 0) || (col == 79)) memory[(row * 80) + col] <= SIDEWALL;
					else memory[(row * 80) + col] <= BRICK;
				end
			end
			// All but the final two rows are empty:
			for (row = 16; row < 58; row = row + 1) begin
				for (col = 0; col < 80; col = col + 1) begin
					if ((col == 0) || (col == 79)) memory[(row * 80) + col] <= SIDEWALL;
					else memory[(row * 80) + col] <= BACKGND;
				end
			end
			// The second-to-last row is where the ball starts:
			for (row = 58; row < 59; row = row + 1) begin
				for (col = 0; col < 80; col = col + 1) begin
					if ((col == 0) || (col == 79)) memory[(row * 80) + col] <= SIDEWALL;
					else if (col == 3) memory[(row * 80) + col] <= BALL;
					else memory[(row * 80) + col] <= BACKGND;
				end
			end
			// The final row contains the paddle:
			for (row = 59; row < 60; row = row + 1) begin
				for (col = 0; col < 80; col = col + 1) begin
					if ((col == 0) || (col == 79)) memory[(row * 80) + col] <= SIDEWALL;
					else if (col < 4) memory[(row * 80) + col] <= PADDLE;
					else memory[(row * 80) + col] <= BACKGND;
				end
			end
		end
		
		// Partial reset returns the last two lines to initial after losing a ball:
		else if (partrst) begin
			// The second-to-last row is where the ball starts:
			for (row = 58; row < 59; row = row + 1) begin
				for (col = 0; col < 80; col = col + 1) begin
					if ((col == 0) || (col == 79)) memory[(row * 80) + col] <= SIDEWALL;
					else if (col == 3) memory[(row * 80) + col] <= BALL;
					else memory[(row * 80) + col] <= BACKGND;
				end
			end
			// The final row contains the paddle:
			for (row = 59; row < 60; row = row + 1) begin
				for (col = 0; col < 80; col = col + 1) begin
					if ((col == 0) || (col == 79)) memory[(row * 80) + col] <= SIDEWALL;
					else if (col < 4) memory[(row * 80) + col] <= PADDLE;
					else memory[(row * 80) + col] <= BACKGND;
				end
			end
		end
		
		// If the reset is not required, then write like normal (if enabled):
		else if (wen_reg) memory[waddr_reg] <= wbus_reg;
		
		// Delay the write signals:
		waddr_reg <= waddr;
		wbus_reg <= wbus;
		wen_reg <= wen;
	end
	
	// Delay the read addresses:
	reg [12:0] raddr_reg_1, raddr_reg_2;
	
	// Read data at the positive edge of the read clock:
	always @ (posedge rclk1) begin
		rbus1 <= memory[raddr_reg_1];
		raddr_reg_1 <= raddr1;
	end
	always @ (posedge rclk2) begin
		rbus2 <= memory[raddr_reg_2];
		raddr_reg_2 <= raddr2;
	end
	
	// Initial memory state:
	initial begin
		// Top row is a solid top wall:
		for (row = 0; row < 1; row = row + 1) begin
			for (col = 0; col < 80; col = col + 1) begin
				memory[(row * 80) + col] <= TOPWALL;
			end
		end
		// Next 5 rows are empty, but place side walls at the side:
		for (row = 1; row < 6; row = row + 1) begin
			for (col = 0; col < 80; col = col + 1) begin
				if ((col == 0) || (col == 79)) memory[(row * 80) + col] <= SIDEWALL;
				else memory[(row * 80) + col] <= BACKGND;
			end
		end
		// Next 10 rows are filled with bricks:
		for (row = 6; row < 16; row = row + 1) begin
			for (col = 0; col < 80; col = col + 1) begin
				if ((col == 0) || (col == 79)) memory[(row * 80) + col] <= SIDEWALL;
				else memory[(row * 80) + col] <= BRICK;
			end
		end
		// All but the final two rows are empty:
		for (row = 16; row < 58; row = row + 1) begin
			for (col = 0; col < 80; col = col + 1) begin
				if ((col == 0) || (col == 79)) memory[(row * 80) + col] <= SIDEWALL;
				else memory[(row * 80) + col] <= BACKGND;
			end
		end
		// The second-to-last row is where the ball starts:
		for (row = 58; row < 59; row = row + 1) begin
			for (col = 0; col < 80; col = col + 1) begin
				if ((col == 0) || (col == 79)) memory[(row * 80) + col] <= SIDEWALL;
				else if (col == 3) memory[(row * 80) + col] <= BALL;
				else memory[(row * 80) + col] <= BACKGND;
			end
		end
		// The final row contains the paddle:
		for (row = 59; row < 60; row = row + 1) begin
			for (col = 0; col < 80; col = col + 1) begin
				if ((col == 0) || (col == 79)) memory[(row * 80) + col] <= SIDEWALL;
				else if (col < 4) memory[(row * 80) + col] <= PADDLE;
				else memory[(row * 80) + col] <= BACKGND;
			end
		end
	end
endmodule
