module screencolor (
	input CLOCK_50,
	input RST_n,
	input R_SW,
	input B_SW,
	input G_SW,
	output [12:0] waddr,
	output [2:0] wbus,
	output wen
);
	// Increment the memory counter at each clock pulse:
	/*reg [18:0] ADDR;
	always @ (negedge CLOCK_50, negedge RST_n) begin
		if (!RST_n) ADDR<=19'd0;
		else ADDR<=ADDR+1;
	end*/
	
	// Write a single bit for each of R, G, B at the least-significant end of the word:
	/*wire [7:0] data;
	assign data = {5'b0, R_SW, B_SW, G_SW};*/	// Stored color is based on switches
	
	// Write the current color data to the memory at rising edge of the reference clock:
	/*game_img_writer img_writer (
		.address (ADDR),
		.clock (CLOCK_50),
		.data (data),
		.wren (1'b1)
	);*/
	
	// Pixel Counter:
	reg [17:0] px;
	always @ (posedge CLOCK_50) begin
		case (px)
			18'd255999: px <= 18'b0;
			default: px <= px + 18'b1;
		endcase
	end
	
	// Drive a single bit for each color:
	wire [2:0] data;
	assign data = {R_SW, B_SW, G_SW};
	
	// Write the current color selection to the current pixel memory:
	assign wen = 1'b1;
	assign waddr = px >> 4;
	assign wbus = data;
endmodule
