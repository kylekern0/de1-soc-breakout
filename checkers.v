/* Verilog HDL Breakout
 * checkers.v
 * Prints a checkerboard pattern to the display
 *
 * Author: kakern16@my.trine.edu
 * Updated: 22 April 2019 */
 
module checkers (
	// Clocks:
	input clk,
	
	// Outputs:
	output wen,
	output reg [12:0] waddr,
	output reg [2:0] wbus
);
	assign wen = 1'b1;
	
	always @ (posedge clk) begin
		// Reset to block address 0 after writing 4000 of them (80x50)
		if (waddr == 13'd4000) begin
			waddr <= 0;
		end
		
		// Update the address and data value:
		waddr <= waddr + 13'd1;
		wbus <= (wbus == 3'd0) ? 3'd7 : 3'd0;
		//wbus <= waddr[2:0];
	end
endmodule
		