/* DE1-SoC Breakout
 * vga_controller.v
 * Displays the 80x50 block game board on a 640x400 pixel VGA display
 * 
 * Author:	kakern16@my.trine.edu
 * Based on a video sync signal generator by Altera */

module vga_controller(
	input iRST_n,				// Reset signal
	input iVGA_CLK,			// 50 MHz reference clock
	input [2:0] rbus,			// Game data memory read bus
	output reg oBLANK_n,		// VGA Blank signal
	output reg oHS,			// VGA horizontal sync signal
	output reg oVS,			// VGA vertical sync signal
	output [7:0] b_data,		// VGA Blue data bus
	output [7:0] g_data,		// VGA Green data bus
	output [7:0] r_data,		// VGA Red data bus
	output reg [12:0] raddr	// Game data memory read address
);
	// Internal signals:
	wire VGA_CLK_n;
	wire cBLANK_n,cHS,cVS,rst;
	wire iBLANK_n,iHS,iVS;

	// Generate the Horizontal and Vertical Sync signals:
	assign rst = ~iRST_n;
	video_sync_generator LTM_ins (
		.vga_clk(iVGA_CLK),
		.reset(rst),
		.blank_n(cBLANK_n),
		.HS(cHS),
		.VS(cVS)
	);
	
	// Count the number of lines we've spent in this row:
	reg [3:0] lines;
	always @ (negedge cHS) begin
		if (cVS) begin
			case (lines)
				4'd7: lines <= 4'd0;
				default: lines <= lines + 4'd1;
			endcase
		end
	end
	
	
	reg [3:0] px_block;
	reg [7:0] buf_block;
	always @ (negedge iVGA_CLK) begin
		// Count the number of pixels we've spent in this column:
		if (cHS && cVS) begin
			case (px_block)
				4'd7: px_block <= 4'd0;
				default: px_block <= px_block + 4'd1;
			endcase
		end
		else px_block <= 4'd0;
		
		// Count columns:
		if (cHS == 1'b0) buf_block <= 8'd0;                  // Reset when horizontal blank happens
		if (px_block == 4'd7) buf_block <= buf_block + 8'd1; // Otherwise, count normally after every 8 real pixels
		if (buf_block == 8'd80) buf_block <= 8'd0;           // Reset to 0 after 80 columns
	end
	
	// Display buffers and control registers:
	reg [2:0] buffer [1:0] [79:0];	// Create the read and disp buffers to store one line of 80 blocks each
	reg read;								// The 'read' bit indicates which buffer (0 or 1) is currently being read into
	
	// Switch the buffers' roles when we begin a new row:
	always @ (lines) if (lines == 4'd0) read <= ~ read;
	
	// Update the memory address we read:
	always @ (negedge iVGA_CLK) begin
		// Reset when the vertical blank happens:
		if (cVS == 1'b0) raddr <= 13'd0;
		
		// Only update the memory address on the first line of the row:
		// This prevents overwriting a row that never gets printed
		if (lines == 4'd0) begin
			case (raddr)
				13'd3999: raddr <= 13'd0;
				default: raddr <= raddr + 13'd1;
			endcase
		end
	end
	
	/*
	// Update the buffer address:
	reg [7:0] buf_block;
	always @ (negedge iVGA_CLK) begin
		// Reset when the horizontal blank happens:
		if (cHS == 1'b0) buf_block <= 8'd0;
		
		// Otherwise, count normally:
		case (buf_block)
			8'd79: buf_block <= 8'd0;
			default: buf_block <= buf_block + 8'd1;
		endcase
	end
	*/
	
	// Read data from the memory into the read buffer:
	always @ (posedge iVGA_CLK) begin
		// Only read if we're allowed to continue:
		if (lines == 4'd0) begin
			buffer[read][buf_block] <= rbus;
		end
	end
	
	// Drive the VGA color data outputs:
	assign r_data = {8{buffer[~read][buf_block][2]}};
	assign g_data = {8{buffer[~read][buf_block][1]}};
	assign b_data = {8{buffer[~read][buf_block][0]}};
	
	// Output the sync signals:
	always @ (negedge iVGA_CLK) begin	// This must be negedge
		oHS <= cHS;
		oVS <= cVS;
		oBLANK_n <= cBLANK_n;
	end
endmodule
