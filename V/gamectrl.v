
/* DE1 SoC Breakout
 * A version of the arcade game "Breakout" for the DE1 development board
 * Written by Kyle Kern <kakern16@my.trine.edu>
 * 
 * gamectrl.v:
 *   Breakout game logic */

module gamectrl (
	// External clock sources:
	input             iVGA_CLK,     // VGA Clock
	
	// Game controls:
	// Note: Buttons are normally high
	input             iReset_Btn,   // HW button used to restart the game
	input             iLeft_Btn,    // HW button used to move the paddle to the left
	input             iLaunch_Btn,  // HW button used to start the game
	input             iRight_Btn,   // HW button used to move the paddle to the right
	
	// Game memory read interface:
	input      [ 2:0] iR_Bus,       // Bus for data read from the memory
	output reg [12:0] oR_Addr,      // Memory address to read
	
	// Game memory write interface:
	output reg [ 2:0] oW_Bus,       // Bus for data to write to the memory
	output reg [12:0] oW_Addr,      // Memory address to write
	output reg        oW_En,        // Memory write enable
	
	// Game state outputs:
	output reg [ 1:0] oState,       // Current state of the game (waiting, active, over)
	output reg [19:0] oScore,       // Current game score
	output reg [ 1:0] oBalls,       // Current number of balls remaining
	output reg        oP_Rst        // Signals the memory controller to reset only the last two rows
);
	// Game state parameters to output to the segment display controller:
	parameter [1:0] GS_PAUSED = 2'b00;  // Awaiting user LAUNCH input; prompt user to "PrESS LAuncH"
	parameter [1:0] GS_ACTIVE = 2'b01;  // Gameplay is active; display the score
	parameter [1:0] GS_OVER   = 2'b10;  // All balls lost; prompt user to "PrESS rESEt"
	
	// Game parameters known by the game controller:
	reg [12:0] Ball_Location;     // Pixel address for the ball
	reg [ 3:0] Ball_Direction;    /* Bit 3: 0 = Held, 1 = Moving
	                               * Bit 2: 0 = Down, 1 = Up
	                               * Bit 1: 0 = No horizontal movement, 1 = Horizontal movement
	                               * Bit 0: 0 = Left, 1 = Right */
	reg [12:0] Paddle_Left_Loc;   // Pixel address for the left end of the paddle
	reg [12:0] Paddle_Right_Loc;  // Pixel address for the right end of the paddle
	reg [ 9:0] Bricks_Remaining;  // Count of the bricks remaining on screen
	
	// Game parameters calculated by the game controller:
	reg [12:0] Ball_Location_Next;
	reg [12:0] Paddle_Left_Loc_Next;
	reg [12:0] Paddle_Right_Loc_Next;
	
	// Since we can only read or write to memory one pixel at a time, the game update process must span multiple clock cycles
	// This is okay, since the screen update is MUCH slower than the VGA signal.
	// However, we will need an internal state variable to keep track of what we need to do next.
	reg [ 7:0] Refresh_State;
	parameter [7:0] RS_MV_PAD_PREPARE   = 8'h10;
	parameter [7:0] RS_MV_PAD_L_READ    = 8'h11;
	parameter [7:0] RS_MV_PAD_L_WAIT    = 8'h12;
	parameter [7:0] RS_MV_PAD_L_TEST    = 8'h13;
	parameter [7:0] RS_MV_PAD_L_ERASE   = 8'h14;
	parameter [7:0] RS_MV_PAD_R_READ    = 8'h15;
	parameter [7:0] RS_MV_PAD_R_WAIT    = 8'h16;
	parameter [7:0] RS_MV_PAD_R_TEST    = 8'h17;
	parameter [7:0] RS_MV_PAD_R_ERASE   = 8'h18;
	parameter [7:0] RS_MV_PAD_DRAW_L    = 8'h19;
	parameter [7:0] RS_MV_PAD_DRAW_R    = 8'h1A;
	parameter [7:0] RS_MV_PAD_FINISH    = 8'h1B;
	parameter [7:0] RS_MV_BALL_PREPARE  = 8'h30;
	parameter [7:0] RS_MV_BALL_HELD     = 8'h31;
	parameter [7:0] RS_MV_BALL_MOVING   = 8'h40;
	parameter [7:0] RS_MV_BALL_READ     = 8'h41;
	parameter [7:0] RS_MV_BALL_WAIT     = 8'h42;
	parameter [7:0] RS_MV_BALL_TEST     = 8'h43;
	parameter [7:0] RS_MV_BALL_ERASE    = 8'h60;
	parameter [7:0] RS_MV_BALL_DRAW     = 8'h61;
	parameter [7:0] RS_MV_BALL_FINISH   = 8'h62;
	parameter [7:0] RS_AWAITING_REFRESH = 8'hFF;
	
	// Game object definitions:
	parameter [2:0] GO_BKGND  = 3'b000;
	parameter [2:0] GO_SWALL  = 3'b001;
	parameter [2:0] GO_TWALL  = 3'b010;
	parameter [2:0] GO_BRICK  = 3'b101;
	parameter [2:0] GO_BALL   = 3'b110;
	parameter [2:0] GO_PADDLE = 3'b111;
	
	// We'll use a "frame rate" of 10 FPS, so we need to refresh every 1/10 second
	// Using the 25 MHz iVGA_CLK as a reference, that translates to 2500000 cycles
	// This timer will track VGA clock pulses so that we know when to refresh again:
	reg [31:0] Refresh_Timer;
	always @ (posedge iVGA_CLK) begin
		case (Refresh_Timer)
			31'd2500000: Refresh_Timer <= 31'd0;
			default: Refresh_Timer <= Refresh_Timer + 31'd1;
		endcase
	end
	
	//always @ (negedge iVGA_CLK or negedge iReset_Btn or posedge Refresh_CLK or negedge Refresh_CLK_Prev) begin
	//always @ (*) begin
	always @ (negedge iVGA_CLK) begin
		// A reset event takes priority over the state structure:
		if (iReset_Btn == 1'b0) begin
			if (Bricks_Remaining != 10'd780) begin
				oScore = 20'd0;
			end
			oState           = GS_PAUSED;  // The game begins paused, waiting for the LAUNCH button
			oBalls           =  2'd3;      // Reset the balls remaining to 3
			Ball_Location    = 13'd4643;   // The ball begins on the 4th column of the 59th row, (58*80)+3
			Ball_Direction   =  4'b0111;   // The ball begins stationary, ready to move up and to the left
			Paddle_Left_Loc  = 13'd4721;   // The paddle's left end begins on the 2nd column of the 60th row, (59*80)+1
			Paddle_Right_Loc = 13'd4723;   // The paddle's right end is two columns away from the left end
			Bricks_Remaining = 10'd0780;   // The game begins with 780 bricks on-screen
			Refresh_State    = RS_AWAITING_REFRESH;
		end
		
		// Un-pause the game when the LAUNCH button is pressed:
		else if (iLaunch_Btn == 1'b0) begin
			// Only allow the launch button to function if the game is not over:
			if (oState != GS_OVER) begin
				oState            = GS_ACTIVE;  // Unpause
				Ball_Direction[3] = 1'b1;       // Allow the ball to move independently
				Refresh_State     = RS_AWAITING_REFRESH;
			end
		end
		
		// If we're due for another screen update, clear the appropriate registers:
		else if (Refresh_Timer == 31'd2500000) begin
			Refresh_State = RS_MV_PAD_PREPARE;
		end
		
		// Otherwise, perform the next needed refresh operation:
		else begin
			// Don't allow anything to happen if the game is over:
			if (oState == GS_OVER) Refresh_State = RS_AWAITING_REFRESH;
			
			// Prepare to move the paddle:
			else if (Refresh_State == RS_MV_PAD_PREPARE) begin
				// Begin by assuming no change in location:
				Paddle_Left_Loc_Next = Paddle_Left_Loc;
				Paddle_Right_Loc_Next = Paddle_Right_Loc;
				// Branch based on selected direction (check buttons):
				if (iLeft_Btn == 1'd0) Refresh_State = RS_MV_PAD_L_READ;        // Move left
				else if (iRight_Btn == 1'd0) Refresh_State = RS_MV_PAD_R_READ;  // Move right
				else Refresh_State = RS_MV_BALL_PREPARE;                        // No move - skip this step
			end
			
			// Read the contents of the paddle's destination pixel when moving left:
			else if (Refresh_State == RS_MV_PAD_L_READ) begin
				Paddle_Left_Loc_Next = Paddle_Left_Loc - 1;  // Calculate the next location
				oR_Addr = Paddle_Left_Loc_Next;              // Set the memory address to read to the above-calculated location
				Refresh_State = RS_MV_PAD_L_WAIT;            // Advance to evaluating the destination contents
			end
			
			// Delay the test process so that the memory has time to read the requested data:
			else if (Refresh_State == RS_MV_PAD_L_WAIT) begin
				Refresh_State = RS_MV_PAD_L_TEST;
			end
			
			// Decide how to move the paddle left based on the destination pixel's contents:
			else if (Refresh_State == RS_MV_PAD_L_TEST) begin
				// If the destination is a side wall (blue), abort:
				if (iR_Bus == GO_SWALL) begin
					Paddle_Left_Loc_Next = Paddle_Left_Loc;  // Reset to the old leftmost location
					Refresh_State = RS_MV_BALL_PREPARE;      // Abort to moving the ball
				end
				
				// Otherwise, proceed to moving the paddle left:
				else begin
					// If we want to allow the player to hit the ball up by running into it, put that here
					Paddle_Right_Loc_Next = Paddle_Right_Loc - 1;  // Also move the right end to the left
					Refresh_State = RS_MV_PAD_L_ERASE;             // Advance to "erasing" the old paddle on screen
				end
			end
			
			// Erase the rightmost paddle pixel when moving it left:
			else if (Refresh_State == RS_MV_PAD_L_ERASE) begin
				oW_En = 1'b1;                      // Enable memory write
				oW_Addr = Paddle_Right_Loc;        // Act on the memory location of the old paddle pixel
				oW_Bus = GO_BKGND;                 // Overwrite the current contents with background (black)
				Refresh_State = RS_MV_PAD_DRAW_L;  // Advance to drawing the new paddle left end to the screen
			end
			
			// Read the contents of the paddle's destination pixel when moving right:
			else if (Refresh_State == RS_MV_PAD_R_READ) begin
				Paddle_Right_Loc_Next = Paddle_Right_Loc + 1;  // Calculate the next location
				oR_Addr = Paddle_Right_Loc_Next;               // Set the memory address to read to the above-calculated location
				Refresh_State = RS_MV_PAD_R_WAIT;              // Advance to evaluating the destination contents
			end
			
			// Delay the test process so that the memory has time to read the requested data:
			else if (Refresh_State == RS_MV_PAD_R_WAIT) begin
				Refresh_State = RS_MV_PAD_R_TEST;
			end
			
			// Decide how to move the paddle right based on the destination pixel's contents:
			else if (Refresh_State == RS_MV_PAD_R_TEST) begin
				// If the destination is a side wall (blue), abort:
				if (iR_Bus == GO_SWALL) begin
					Paddle_Right_Loc_Next = Paddle_Right_Loc;  // Reset to the old rightmost location
					Refresh_State = RS_MV_BALL_PREPARE;        // Abort to moving the ball
				end
				
				// Otherwise, proceed to moving the paddle right:
				else begin
					// If we want to allow the player to hit the ball up by running into it, put that here
					Paddle_Left_Loc_Next = Paddle_Left_Loc + 1;  // Also move the left end to the right
					Refresh_State = RS_MV_PAD_R_ERASE;           // Advance to "erasing" the old paddle on the screen
				end
			end
			
			// Erase the leftmost paddle pixel when moving it right:
			else if (Refresh_State == RS_MV_PAD_R_ERASE) begin
				oW_En = 1'b1;                      // Enable memory write
				oW_Addr = Paddle_Left_Loc;         // Act on the memory location of the old paddle pixel
				oW_Bus = GO_BKGND;                 // Overwrite the current memory contents with background (black)
				Refresh_State = RS_MV_PAD_DRAW_L;  // Advance to drawing the new paddle left end to the screen
			end
			
			// Write left end pixel to the display:
			else if (Refresh_State == RS_MV_PAD_DRAW_L) begin
				// Write is already enabled
				oW_Addr = Paddle_Left_Loc_Next;    // Act on the memory location of the new left end of the paddle
				oW_Bus = GO_PADDLE;                // Write a white paddle pixel to the location
				Refresh_State = RS_MV_PAD_DRAW_R;  // Advance to drawing the new paddle right end to the screen
			end
			
			// Write right end pixel to the display:
			else if (Refresh_State == RS_MV_PAD_DRAW_R) begin
				// Write is already enabled
				oW_Addr = Paddle_Right_Loc_Next;   // Act on the memory location of the new right end of the paddle
				oW_Bus = GO_PADDLE;                // Write a white paddle pixel to the location
				Refresh_State = RS_MV_PAD_FINISH;  // Advance to finalizing the paddle-moving process
			end
			
			// Finalize the paddle-moving process by setting the new location as the current location:
			else if (Refresh_State == RS_MV_PAD_FINISH) begin
				oW_En = 1'b0;
				Paddle_Left_Loc = Paddle_Left_Loc_Next;
				Paddle_Right_Loc = Paddle_Right_Loc_Next;
				Refresh_State = RS_MV_BALL_PREPARE;        // Advance to beginning to move the ball
			end
			
			// Prepare to move the ball:
			else if (Refresh_State == RS_MV_BALL_PREPARE) begin
				// Begin by assuming no change in location:
				Ball_Location_Next = Ball_Location;
				// The ball moves differently depending on whether or not it's held:
				if (Ball_Direction[3] == 1'b0) Refresh_State = RS_MV_BALL_HELD;
				else Refresh_State = RS_MV_BALL_MOVING;
			end
			
			// Calculate the ball's new location when held:
			else if (Refresh_State == RS_MV_BALL_HELD) begin
				// When held, the ball always appears over the right end of the paddle:
				Ball_Location_Next = Paddle_Right_Loc_Next - 13'd80;
				// If the paddle hasn't moved, we needn't move the ball; abort the remainder of this update procedure:
				if (Ball_Location_Next == Ball_Location) Refresh_State = RS_AWAITING_REFRESH;
				else Refresh_State = RS_MV_BALL_ERASE;
			end
			
			// Calculate the ball's new location when moving:
			else if (Refresh_State == RS_MV_BALL_MOVING) begin
				// The ball always moves vertically; bit 2 of Ball_Direction determines up vs. down:
				if (Ball_Direction[2] == 1'b0) Ball_Location_Next = Ball_Location_Next + 13'd80;  // Move the ball down one row (forward 80px)
				else Ball_Location_Next = Ball_Location_Next - 13'd80;                            // Move the ball up one row (backward 80px)
				
				// The ball moves horizontally iff bit 1 of Ball_Direction is high:
				if (Ball_Direction[1] == 1'b1) begin
					if (Ball_Direction[0] == 1'b0) Ball_Location_Next = Ball_Location_Next - 13'd1;  // Move the ball left one column (backward 1px)
					else Ball_Location_Next = Ball_Location_Next + 13'd1;                            // Move the ball right one column (forward 1px)
				end
				
				Refresh_State = RS_MV_BALL_READ;  // Advance to reading the contents of the ball's destination pixel
			end
			
			// Read the contents of the ball's destination pixel:
			else if (Refresh_State == RS_MV_BALL_READ) begin
				oR_Addr = Ball_Location_Next;     // Set the memory address to read to the calculated next ball location
				Refresh_State = RS_MV_BALL_WAIT;  // Advance to (waiting to) evaluate the destination contents
			end
			
			// Delay the test process so that the memory has time to read the requested data:
			else if (Refresh_State == RS_MV_BALL_WAIT) begin
				Refresh_State = RS_MV_BALL_TEST;
			end
			
			// Read and evaluate the contents of the ball's destination pixel:
			else if (Refresh_State == RS_MV_BALL_TEST) begin
				case (iR_Bus)					
					// The destination is a side wall:
					GO_SWALL: begin
						Ball_Direction[0] = ~ Ball_Direction[0];  // Side walls reverse the ball's horizontal direction
						Refresh_State = RS_MV_BALL_PREPARE;       // Having changed the direction, we must recalculate the destination
					end
					
					// The destination is the top wall:
					GO_TWALL: begin
						Ball_Direction[2] = 1'd0;            // The ball can only bounce down off the top wall
						Refresh_State = RS_MV_BALL_PREPARE;  // Having changed the direction, we must recalculate the destination
					end
					
					// The destination is a brick:
					GO_BRICK: begin
						oScore = oScore + 20'd1;                      // Increase the score by 1
						Bricks_Remaining = Bricks_Remaining - 10'd1;  // Decrease on-screen brick count by 1
						
						// When no bricks are left, start a new game without clearing the score:
						if (Bricks_Remaining == 10'd0) begin
							Bricks_Remaining = 10'd780;   // Reset the number of bricks remaining
							oState           = GS_OVER;
							Ball_Location    = 13'd4643;
							Ball_Direction   =  4'b0111;
							Paddle_Left_Loc  = 13'd4721;
							Paddle_Right_Loc = 13'd4723;
							Refresh_State = RS_AWAITING_REFRESH;
						end
						else Refresh_State = RS_MV_BALL_ERASE;  // If we don't need a new game, proceed to move the ball on-screen
					end
					
					// The destination is the paddle:
					GO_PADDLE: begin
						Ball_Direction[2] = 1'd1;  // The ball can only bounce up off the paddle
						
						// If the ball runs into a moving paddle, we change the horizontal direction:
						if ((iLeft_Btn == 1'd0) || (iRight_Btn == 1'd0)) begin
							// If the ball direction has no horizontal component, now it does:
							if (Ball_Direction[1] == 1'd0) begin
								Ball_Direction[1] = 1'd1;
								if (iLeft_Btn == 1'd0) Ball_Direction[0] = 1'd0;  // If the paddle is moving left, now the ball is moving left
								else Ball_Direction[0] = 1'd1;                    // If the paddle is moving right, now the ball is moving right
							end
							
							// If the ball direction does have a horizontal component, contrary paddle motion "slows" it to vertical:
							else begin
								if ((Ball_Direction[0] == 1'd0) && (iRight_Btn == 1'd0)) Ball_Direction[1] = 1'd0;
								if ((Ball_Direction[0] == 1'd1) && (iLeft_Btn  == 1'd0)) Ball_Direction[1] = 1'd0;
							end
						end
						
						Refresh_State = RS_MV_BALL_PREPARE;  // Having changed the direction, we must recalculate the destination
					end
					
					// If no special action is required (Ex. the ball is moving into empty background):
					default: Refresh_State = RS_MV_BALL_ERASE;  // Just move on to moving the ball
				endcase
			end
			
			// Erase the old ball pixel:
			else if (Refresh_State == RS_MV_BALL_ERASE) begin
				oW_En = 1'b1;                     // Enable memory write
				oW_Addr = Ball_Location;          // Act on the memory address of the old ball pixel
				oW_Bus = 3'd000;                  // Write a black background pixel to the location
				Refresh_State = RS_MV_BALL_DRAW;  // Advance to writing the new ball
			end
			
			// Draw the new ball pixel:
			else if (Refresh_State == RS_MV_BALL_DRAW) begin
				// Write is already enabled
				oW_Addr = Ball_Location_Next;       // Act on the memory address of the new ball pixel
				oW_Bus = 3'd110;                    // Write a yellow ball pixel to the location
				Refresh_State = RS_MV_BALL_FINISH;  // Advance to finalizing the ball-moving process
			end
			
			// Finalize the ball-moving process by setting the new location as the current location:
			else if (Refresh_State == RS_MV_BALL_FINISH) begin
				oW_En = 1'b0;
				Ball_Location = Ball_Location_Next;
				Refresh_State = RS_AWAITING_REFRESH;  // Nothing else to do until it's time to refresh again
				
				// Now check to see if the ball landed off-screen; if so, lose a ball!
				if (Ball_Location >= 13'd4800) begin
					oBalls = oBalls - 2'd1;
					
					// If there are 0 balls left, it's game over!
					if (oBalls == 2'd0) oState = GS_OVER;
					
					// Otherwsise, we need to reset the game for the next go:
					else begin
						oState           = GS_PAUSED;
						Ball_Location    = 13'd4643;
						Ball_Direction   =  4'b0111;
						Paddle_Left_Loc  = 13'd4721;
						Paddle_Right_Loc = 13'd4723;
						oP_Rst           = 1'd1;       // Tell the memory to reset the last two lines of the display accordingly
					end
				end
			end
			
			else if (Refresh_State == RS_AWAITING_REFRESH) begin
				oP_Rst = 1'd0;  // Don't allow the memory partial reset line to stay high
			end
		end
	end
	
	// Initial register states (same as reset):
	initial begin
		oState           = GS_PAUSED;  // The game begins paused, waiting for the LAUNCH button
		oScore           = 20'd0;      // Reset the score to zero
		oBalls           =  2'd3;      // Reset the balls remaining to 3
		Ball_Location    = 13'd4643;   // The ball begins on the 4th column of the 59th row, (58*80)+3
		Ball_Direction   =  4'b0111;   // The ball begins stationary, ready to move up and to the left
		Paddle_Left_Loc  = 13'd4721;   // The paddle's left end begins on the 2nd column of the 60th row, (59*80)+1
		Paddle_Right_Loc = 13'd4723;   // The paddle's right end is two columns away from the left end
		Bricks_Remaining = 10'd0780;   // The game begins with 780 bricks on-screen
		Refresh_State    = RS_AWAITING_REFRESH;
	end
endmodule
